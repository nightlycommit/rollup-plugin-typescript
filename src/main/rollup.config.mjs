import {createTypeScriptPlugin} from "@vitrail/rollup-plugin-typescript";
import createGeneratePackageJsonPlugin from "rollup-plugin-generate-package-json";
import {dirname} from "path";
import FsExtra from "fs-extra";

const commonjsTargetName = 'index.cjs';
const moduleTargetName = 'index.mjs';
const packageName = '@vitrail/rollup-plugin-typescript';

const {emptyDirSync} = FsExtra;

/**
 * @var {import("rollup").RollupOptionsFunction}
 */
const program = (commandLineArgs) => {
    const version = process.env.VERSION || '0.0.0-SNAPSHOT';

    console.log(`Building main artifact under version ${version}...`);

    /**
     * @var {Array<import("rollup").RollupOptions>}
     */
    const options = [{
        input: 'lib.ts',
        plugins: [
            {
                generateBundle: (options) => {
                    const destination = dirname(options.file);

                    emptyDirSync(destination);
                }
            },
            createTypeScriptPlugin(),
            createGeneratePackageJsonPlugin({
                inputFolder: '../..',
                baseContents: {
                    main: commonjsTargetName,
                    module: moduleTargetName,
                    name: packageName,
                    types: `lib.d.ts`,
                    repository: {
                        url: 'https://gitlab.com/vitrail/rollup-plugin-typescript'
                    },
                    bugs: {
                        url: 'https://gitlab.com/vitrail/rollup-plugin-typescript/-/issues'
                    },
                    homepage: 'https://rollup-plugin-typescript.nightlycommit.com',
                    author: 'Eric MORAND <eric.morand@gmail.com>',
                    license: 'BSD-3-Clause',
                    keywords: [
                        'rollup-plugin',
                        'typescript'
                    ],
                    peerDependencies: {
                        tslib: "^2.6.2",
                        typescript: "^5.3.3"
                    },
                    version
                },
                additionalDependencies: []
            }),
        ],
        output: {
            file: 'target/index.cjs',
            format: "commonjs"
        }
    },
        {
            input: 'lib.ts',
            plugins: [
                createTypeScriptPlugin({
                    target: "es2015"
                })
            ],
            output: {
                file: 'target/index.mjs',
                format: "esm"
            }
        }
    ];

    return options;
};

export default program;