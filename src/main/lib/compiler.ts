import ts from "typescript";
import {dirname} from "path";
import {isAMapOutputFile, isACodeOutputFile} from "./helpers";
import type {Filesystem} from "./filesystem";

type Error = {
    column: number;
    file: string;
    line: number;
    message: string;
} | string;

export type CompilationErrors = Array<Error>;

export interface Compiler {
    compile(fileName: string): CompilationErrors;

    getOutputFileNames(fileName: string): {
        code: string;
        map: string;
    };
}

const createErrorFromDiagnostic = (diagnostic: ts.Diagnostic): Error => {
    const message = ts.flattenDiagnosticMessageText(diagnostic.messageText, ts.sys.newLine);

    if (diagnostic.file) {
        const {line, character} = ts.getLineAndCharacterOfPosition(diagnostic.file, diagnostic.start!);

        return {
            column: character,
            file: diagnostic.file.fileName,
            message,
            line: line + 1
        };
    }
    else {
        return message;
    }
};

export const createCompiler = (
    options: ts.CompilerOptions,
    fileSystem: Filesystem,
    log: typeof console.log
): Compiler => {
    const tsSystem = ts.sys;

    const system: ts.System = {
        ...tsSystem,
        fileExists: (path) => {
            return fileSystem.existsSync(path) || tsSystem.fileExists(path);
        },
        readFile: (path, encoding?) => {
            if (fileSystem.existsSync(path)) {
                return fileSystem.readFileSync(path, encoding).toString();
            }

            return tsSystem.readFile(path, encoding);
        },
        writeFile: (path, data) => {
            const parentPath = dirname(path);

            if (!fileSystem.existsSync(parentPath)) {
                fileSystem.mkdirSync(parentPath);
            }

            return fileSystem.writeFileSync(path, data);
        }
    };

    const host = ts.createIncrementalCompilerHost(
        options,
        system
    );

    const createProgram = (
        fileName: string,
        options: ts.CompilerOptions,
        host: ts.CompilerHost
    ) => {
        return ts.createIncrementalProgram({
            rootNames: [
                fileName
            ],
            options,
            host
        });
    };

    return {
        compile: (fileName) => {
            log(`Compile`, fileName);
            
            const program = createProgram(fileName, options, host);

            // options diagnostics
            const optionsDiagnostics = program.getOptionsDiagnostics();

            if (optionsDiagnostics.length > 0) {
                return optionsDiagnostics.map(createErrorFromDiagnostic);
            }

            const errors: Array<Error> = [];

            let done: boolean = false;

            while (!done) {
                const emitResult = program.emitNextAffectedFile();

                const diagnostics = emitResult?.result.diagnostics;

                if (diagnostics) {
                    errors.push(...diagnostics.map(createErrorFromDiagnostic));
                }

                done = (emitResult === undefined);
            }

            return errors;
        },
        getOutputFileNames: (fileName) => {
            log(`getOutputFileNames`, fileName);

            const outputFileNames = ts.getOutputFileNames({
                options,
                fileNames: [
                    fileName.replace(/\\/g, '/')
                ],
                errors: []
            }, fileName, !ts.sys.useCaseSensitiveFileNames);

            return {
                code: outputFileNames.find(isACodeOutputFile)!,
                map: outputFileNames.find(isAMapOutputFile)!
            };
        }
    }
};
