export interface Filesystem {
    existsSync(path: string): boolean;
    
    getFiles(): Array<{
        path: string;
        content: string | null;
    }>;

    /**
     * Creates a directory, recursively.
     */
    mkdirSync(path: string): void;
    
    readFileSync(path: string, encoding?: string): Buffer | string;
    
    writeFileSync(path: string, data: string): void;
    
    rmSync(path: string): void;
}