/**
 * Checks if the given OutputFile represents some declaration
 */
export const isADeclarationOutputFile = (name: string): boolean => {
    return /\.d\.[cm]?ts$/.test(name);
};

/**
 * Checks if the given OutputFile represents some code
 */
export const isACodeOutputFile = (name: string): boolean => {
    return !isAMapOutputFile(name) && !isADeclarationOutputFile(name);
};

/**
 * Checks if the given OutputFile represents some source map
 */
export const isAMapOutputFile = (name: string): boolean => {
    return name.endsWith('.map');
};