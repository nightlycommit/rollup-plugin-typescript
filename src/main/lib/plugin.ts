import type {Plugin, SourceDescription} from "rollup";
import typeScript from "typescript";
import {createFilter} from "@rollup/pluginutils";
import {dirname, normalize, relative, resolve} from "node:path";
import {CompilationErrors, createCompiler} from "./compiler";
import {Volume} from "memfs";
import {isADeclarationOutputFile} from "./helpers";
import type {Filesystem} from "./filesystem";

export type FactoryOptions = {
    /**
     * An instance of TypeScript `CompilerOptions`, minus the properties `inlineSourceMap` and `sourceMap`. Used by the factory to either override the compiler options resolved from the first available `tsconfig.json` file (starting from the current working directory) if any, or as the entire set of compiler options otherwise.
     *
     * Note that the `inlineSourceMap` and `sourceMap` properties will always be passed as `false` and `true`, respectively, to the underlying TypeScript compiler, in order to guarantee that the plugin is always capable of returning a source map to the Rollup engine.
     */
    compilerOptions?: Omit<typeScript.CompilerOptions, "inlineSourceMap" | "sourceMap">;

    /**
     * A pattern, or an array of patterns, which specifies the files in the build the plugin should ignore. By default, no files are ignored.
     */
    exclude?: ReadonlyArray<string | RegExp> | string | RegExp;

    /**
     * A pattern, or an array of patterns, which specifies the files in the build the plugin should operate on. By default, `.ts`, `.cts`, `.mts` and `.tsx` files are targeted.
     */
    include?: ReadonlyArray<string | RegExp> | string | RegExp;

    /**
     * A function executed whenever the plugin needs to log a message.
     */
    log?: typeof console.log;
};

export type TypeScriptPlugin = Omit<Plugin<null>, "transform"> & {
    transform(this: {
        error(error: string, position?: { column: number; line: number }): void;
    }, code: string, id: string): null | SourceDescription;
};

/**
 * Returns a newly created instance of [Plugin](https://rollupjs.org/plugin-development/) from the passed options.
 *
 * @param options
 */
export const createTypeScriptPlugin = (options: FactoryOptions = {}): TypeScriptPlugin => {
    let passedCompilerOptions = options.compilerOptions || {};

    const configFilePath = typeScript.findConfigFile(process.cwd(), typeScript.sys.fileExists);

    const tsConfigFile: {
        compilerOptions: typeScript.CompilerOptions
    } = configFilePath ? typeScript.readConfigFile(configFilePath, typeScript.sys.readFile).config : {};

    const parsedCommandLine = typeScript.parseJsonConfigFileContent(
        {
            ...tsConfigFile,
            compilerOptions: {
                ...tsConfigFile.compilerOptions,
                ...passedCompilerOptions
            }
        },
        typeScript.sys,
        process.cwd()
    );

    const compilerOptions: typeScript.CompilerOptions = {
        ...parsedCommandLine.options,
        inlineSourceMap: false,
        sourceMap: true
    };

    const volume = new Volume();

    const fileSystem: Filesystem = {
        existsSync: (path) => {
            return volume.existsSync(path);
        },
        getFiles: () => {
            const results: ReturnType<Filesystem["getFiles"]> = [];

            const volumeContent = volume.toJSON();

            for (const name in volumeContent) {
                results.push({
                    path: name,
                    content: volumeContent[name]
                });
            }

            return results;
        },
        mkdirSync: (path) => {
            return volume.mkdirSync(path, {
                recursive: true
            });
        },
        readFileSync: (path, encoding) => {
            return volume.readFileSync(path, encoding);
        },
        writeFileSync: (path, data) => {
            return volume.writeFileSync(path, data);
        },
        rmSync: (path) => {
            return volume.rmSync(path);
        }
    };
    const log = options.log || (() => {
    });
    const compiler = createCompiler(compilerOptions, fileSystem, log);

    const createModuleResolver = () => {
        const cache = typeScript.createModuleResolutionCache(
            process.cwd(),
            host.getCanonicalFileName,
            compilerOptions
        );

        return (moduleName: string, containingFile: string, redirectedReference?: typeScript.ResolvedProjectReference, mode?: typeScript.ResolutionMode) => {
            const {resolvedModule} = typeScript.resolveModuleName(
                moduleName,
                containingFile,
                compilerOptions,
                typeScript.sys,
                cache,
                redirectedReference,
                mode
            );

            return resolvedModule;
        };
    };

    const host = typeScript.createCompilerHost(compilerOptions);

    // options
    // @see https://github.com/rollup/plugins/issues/1651#issuecomment-1868495405
    options.include = options.include || /\.(cts|mts|ts|tsx)$/;

    if (typeof options.include === "string") {
        options.include = [options.include];
    }

    if (Array.isArray(options.include)) {
        options.include = options.include.map((include) => {
            if (typeof include === "string") {
                return new RegExp(include);
            }

            return include;
        });
    }

    const filter = createFilter(options.include, options.exclude);
    const resolveModule = createModuleResolver();
    const emittedDeclarationFiles: Array<{
        content: string;
        name: string;
    }> = [];

    let compilationErrors: CompilationErrors = [];

    const plugin: TypeScriptPlugin = {
        name: 'typescript',

        buildStart() {
            compilationErrors = [];
        },

        transform(code, id) {
            if (!filter(id)) {
                return null;
            }

            const resolvedId = id;
            
            if (fileSystem.existsSync(resolvedId)) {
                const cachedCode = fileSystem.readFileSync(resolvedId).toString();

                if (cachedCode !== code) {
                    const outputFileNames = compiler.getOutputFileNames(resolvedId);

                    fileSystem.rmSync(outputFileNames.code);

                    log(`${resolvedId}'s cache invalidated`);
                }
            }

            fileSystem.mkdirSync(dirname(resolvedId));
            fileSystem.writeFileSync(resolvedId, code);

            const getTransformResult = (resolvedId: string): null | SourceDescription => {
                const outputFileNames = compiler.getOutputFileNames(resolvedId);

                const code = fileSystem.existsSync(outputFileNames.code) && fileSystem.readFileSync(outputFileNames.code).toString();

                if (code) {
                    const map = fileSystem.readFileSync(outputFileNames.map).toString();

                    return {code, map};
                }

                return null;
            };

            let result = getTransformResult(resolvedId);

            if (!result) {
                log(`${resolvedId} was encountered for the first time`);

                compilationErrors.push(...compiler.compile(resolvedId));

                log(`compilationErrors are`, compilationErrors);

                const files = fileSystem.getFiles();

                for (const file of files) {
                    if (isADeclarationOutputFile(file.path)) {
                        const content = file.content!;

                        emittedDeclarationFiles.push({
                            name: relative(dirname(id), file.path),
                            content
                        });
                    }
                }

                const errorsToEmit = compilationErrors.filter((error) => {
                    // if error is a string, this is a global error
                    return typeof error === "string" || error.file === resolvedId;
                });

                if (errorsToEmit.length > 0) {
                    for (const error of errorsToEmit) {
                        if (typeof error === "string") {
                            this.error(error);
                        }
                        else {
                            const {column, line, message} = error;

                            this.error(message, {
                                column,
                                line
                            });
                        }
                    }
                }

                result = getTransformResult(resolvedId);
            }

            return result;
        },
        resolveId(importee, importer) {
            if (importer !== undefined) {
                const containingFile = normalize(importer);

                const mode = typeScript.getImpliedNodeFormatForFile(
                    containingFile,
                    undefined,
                    typeScript.sys,
                    compilerOptions
                );

                const resolved = resolveModule(
                    importee,
                    containingFile,
                    undefined,
                    mode
                );

                if (!resolved || isADeclarationOutputFile(resolved.resolvedFileName)) {
                    return null;
                }

                return normalize(resolve(resolved.resolvedFileName));
            }

            return null;
        },
        generateBundle() {
            for (const file of emittedDeclarationFiles) {
                this.emitFile({
                    type: "asset",
                    fileName: file.name,
                    source: file.content
                });
            }
        }
    };

    return plugin;
};

export default createTypeScriptPlugin;