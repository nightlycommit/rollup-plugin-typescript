import tape from "tape";

import {runTest} from "../helpers/index";

tape('filter', ({test}) => {
    test('filters out non supported modules', (test) => {
        runTest(test, 'basic/entry.js', {
            'basic/entry.js': `export const foo = 5;`,
        }, {
            expectation: {
                foo: 5
            }
        });
    });
});