import tape from "tape";
import {createCompiler} from "../../../main/lib/compiler";
import {Volume} from "memfs";
import {spy} from "sinon";
import {resolve} from "path";
import type {Filesystem} from "../../../main/lib/filesystem";

tape('compiler honors incremental', ({same, end}) => {
    const volume = Volume.fromJSON({
        'entry.ts': `export {foo} from "./foo";
export {bar} from "./bar";      
`,
        'foo.ts': `import {bar} from "./bar";
export const foo: number = 5 + bar;`,
        'bar.ts': 'export const bar: number = 5;'
    });

    const fileSystem: Filesystem = {
        existsSync: (path) => {
            return volume.existsSync(path);
        },
        getFiles: () => {
            const results: ReturnType<Filesystem["getFiles"]> = [];

            const volumeContent = volume.toJSON();

            for (const name in volumeContent) {
                results.push({
                    path: name,
                    content: volumeContent[name]
                });
            }

            return results;
        },
        mkdirSync: (path) => {
            return volume.mkdirSync(path, {
                recursive: true
            });
        },
        readFileSync: (path, encoding) => {
            return volume.readFileSync(path, encoding);
        },
        writeFileSync: (path, data) => {
            return volume.writeFileSync(path, data);
        },
        rmSync: (path) => {
            return volume.rmSync(path);
        }
    };
    
    const log = spy(() => {});
    
    const compiler = createCompiler({
        incremental: true,
        tsBuildInfoFile: '.tsbuildinfo',
        noEmitOnError: true
    }, fileSystem, log);

    const writeFileSpy = spy(volume, "writeFileSync");

    compiler.compile('entry.ts');
    
    same(log.getCall(0).args, [ 'Compile', 'entry.ts' ]);
    same(writeFileSpy.getCalls().map((call) => call.firstArg), [
        resolve('bar.js'),
        resolve('foo.js'),
        'entry.js',
        '.tsbuildinfo'
    ]);

    volume.writeFileSync('entry.ts', `export {foo} from "./foo";
export {bar} from "./bar";
export const oof: number = 15;      
`);

    writeFileSpy.resetHistory();

    compiler.compile('entry.ts');

    // at that point, the only way we have to guarantee that incremental compilation happened
    // is to check the order of writeFileSync calls: on incremental compilation, the entry point
    // - which is the only one that was changed - is written first
    same(writeFileSpy.getCalls().map((call) => call.firstArg), [
        'entry.js',
        resolve('bar.js'),
        resolve('foo.js'),
        '.tsbuildinfo'
    ]);

    end();
});