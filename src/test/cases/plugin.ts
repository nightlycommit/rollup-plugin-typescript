import tape from "tape";
import createTypeScriptPlugin from "../../main/lib/plugin";
import {spy} from "sinon";

tape('Plugin', ({test}) => {
    test('TypeScriptPlugin::transform', ({end, same, true: isTrue}) => {
        const log = spy(() => {
        });

        const plugin = createTypeScriptPlugin({
            log
        });

        const resultA = plugin.transform.bind({
            error: () => {
            }
        })('5;', 'foo.ts');

        same(eval(resultA!.code), 5);

        log.resetHistory();
        
        const resultB = plugin.transform.bind({
            error: () => {
            }
        })('10;', 'foo.ts');

        isTrue(log.getCalls().map((call) => call.firstArg).includes('foo.ts\'s cache invalidated'));
        same(eval(resultB!.code), 10);

        log.resetHistory();

        const resultC = plugin.transform.bind({
            error: () => {
            }
        })('10;', 'foo.ts');
        
        same(eval(resultC!.code), 10);

        end();
    });
});