import tape from "tape";
import {runTest} from "../helpers/index";

tape('compilerOptions', ({test}) => {
    test('honors declaration option', (test) => {
        return runTest(test, 'entry.ts', {
            'entry.ts': `import {bar} from "./foo/bar";
export const foo: number = bar;`,
            'foo/bar.ts': `export const bar: number = 5;`
        }, {
            options: {
                compilerOptions: {
                    declaration: true,
                    module: 'ESNext'
                }
            },
            expectation: {
                foo: 5
            },
            expectedOutput: [
                {
                    type: "chunk",
                    name: "entry.js"
                },
                {
                    type: "asset",
                    name: "foo/bar.d.ts"
                },
                {
                    type: "asset",
                    name: "entry.d.ts"
                }
            ]
        });
    });
});