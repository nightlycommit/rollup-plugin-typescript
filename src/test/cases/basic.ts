import tape from "tape";

import {runTest} from "../helpers/index";

tape('basic', ({test}) => {
    test('with a standalone module', (test) => {
        return runTest(test, 'basic/entry.ts', {
            'basic/entry.ts': `export const foo: number = 5;`
        }, {
            expectation: {
                foo: 5
            }
        });
    });

    test('with a module that depends on a local module', (test) => {
        const modules = {
            'basic/entry.ts': `export {foo} from "./foo";`,
            'basic/foo.ts': `export const foo: number = 5;`,
            'tsconfig.json': JSON.stringify({
                compilerOptions: {
                    module: 'ESNext'
                }
            })
        };

        return runTest(test, 'basic/entry.ts', modules, {
            expectation: {
                foo: 5
            }
        });
    });

    test('with a module that depends on a local module located outside of the current working directory', (test) => {
        const modules = {
            'basic/entry.ts': `export {foo} from "../foo";`,
            'foo.ts': `export const foo: number = 5;`,
            'tsconfig.json': JSON.stringify({
                compilerOptions: {
                    module: 'ESNext'
                }
            })
        };

        return runTest(test, 'basic/entry.ts', modules, {
            expectation: {
                foo: 5
            }
        });
    });

    test('with a module that depends on a node module', (test) => {
        return runTest(test, 'basic/entry.ts', {
            'basic/entry.ts': `export {foo} from "foo";`,
            'node_modules/foo.js': `export const foo = 5;`,
            'tsconfig.json': JSON.stringify({
                compilerOptions: {
                    module: 'ESNext',
                    moduleResolution: 'Node'
                }
            })
        }, {
            expectation: {
                foo: 5
            }
        });
    });

    test('with a module that depends on a lot of modules', (test) => {
        const modules: Record<string, string> = {
            'tsconfig.json': JSON.stringify({
                compilerOptions: {
                    module: 'ESNext'
                }
            })
        };

        let entry: string = '';

        for (let i = 0; i < 100; i++) {
            modules[`basic/foo${i}.ts`] = `export const foo${i}: number = ${i};`;

            entry += `export {foo${i}} from "./foo${i}";`;
        }

        modules['basic/entry.ts'] = entry;

        return runTest(test, 'basic/entry.ts', modules, {
            expectation: {
                foo0: 0,
                foo1: 1,
                foo2: 2,
                foo3: 3,
                foo4: 4,
                foo5: 5,
                foo6: 6,
                foo7: 7,
                foo8: 8,
                foo9: 9,
                foo10: 10,
                foo11: 11,
                foo12: 12,
                foo13: 13,
                foo14: 14,
                foo15: 15,
                foo16: 16,
                foo17: 17,
                foo18: 18,
                foo19: 19,
                foo20: 20,
                foo21: 21,
                foo22: 22,
                foo23: 23,
                foo24: 24,
                foo25: 25,
                foo26: 26,
                foo27: 27,
                foo28: 28,
                foo29: 29,
                foo30: 30,
                foo31: 31,
                foo32: 32,
                foo33: 33,
                foo34: 34,
                foo35: 35,
                foo36: 36,
                foo37: 37,
                foo38: 38,
                foo39: 39,
                foo40: 40,
                foo41: 41,
                foo42: 42,
                foo43: 43,
                foo44: 44,
                foo45: 45,
                foo46: 46,
                foo47: 47,
                foo48: 48,
                foo49: 49,
                foo50: 50,
                foo51: 51,
                foo52: 52,
                foo53: 53,
                foo54: 54,
                foo55: 55,
                foo56: 56,
                foo57: 57,
                foo58: 58,
                foo59: 59,
                foo60: 60,
                foo61: 61,
                foo62: 62,
                foo63: 63,
                foo64: 64,
                foo65: 65,
                foo66: 66,
                foo67: 67,
                foo68: 68,
                foo69: 69,
                foo70: 70,
                foo71: 71,
                foo72: 72,
                foo73: 73,
                foo74: 74,
                foo75: 75,
                foo76: 76,
                foo77: 77,
                foo78: 78,
                foo79: 79,
                foo80: 80,
                foo81: 81,
                foo82: 82,
                foo83: 83,
                foo84: 84,
                foo85: 85,
                foo86: 86,
                foo87: 87,
                foo88: 88,
                foo89: 89,
                foo90: 90,
                foo91: 91,
                foo92: 92,
                foo93: 93,
                foo94: 94,
                foo95: 95,
                foo96: 96,
                foo97: 97,
                foo98: 98,
                foo99: 99
            }
        });
    });

    test('with two independent modules', (test) => {
        return runTest(test, [
            'basic/foo.ts',
            'basic/bar.ts'
        ], {
            'basic/foo.ts': `export const foo: number = 5;`,
            'basic/bar.ts': `export const foo: number = 5;`
        }, {
            expectation: {
                foo: 5
            }
        });
    });
});