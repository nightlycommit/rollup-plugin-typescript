import tape from "tape";
import {runTest} from "../../../helpers/index";

tape('ES2022 module compatibility', (test) => {
    return runTest(test, 'entry.ts', {
        'entry.ts': 'export const foo: number = 5;',
        'tsconfig.json': JSON.stringify({
            compilerOptions: {
                module: 'ES2022'
            }
        })
    }, {
        expectation: {
            foo: 5
        }
    });
});