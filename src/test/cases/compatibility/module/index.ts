import "./amd";
import "./common-js";
import "./es-6";
import "./es-2015";
import "./es-2020";
import "./es-2022";
import "./es-next";
import "./index";
import "./node-16";
import "./node-next";
import "./none";
import "./system";
import "./umd";