import tape from "tape";
import {runTest} from "../../../helpers/index";

tape('NodeNext module compatibility', (test) => {
    return runTest(test, 'entry.ts', {
        'entry.ts': 'export const foo: number = 5;',
        'tsconfig.json': JSON.stringify({
            compilerOptions: {
                module: 'NodeNextvv'
            }
        })
    }, {
        expectation: {
            foo: 5
        }
    });
});