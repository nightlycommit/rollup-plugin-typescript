import tape from "tape";
import {runTest} from "../../../helpers/index";

tape('AMD module compatibility', (test) => {
    return runTest(test, 'entry.ts', {
        'entry.ts': 'export const foo: number = 5;',
        'tsconfig.json': JSON.stringify({
            compilerOptions: {
                module: 'AMD'
            }
        })
    }, {
        expectedRuntimeError: {
            message: 'define is not defined'
        }
    });
});