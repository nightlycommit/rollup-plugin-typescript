import tape from "tape";

import {runTest} from "../../../helpers/index";
import {Volume} from "memfs";
import {stub} from "sinon";

tape('backslashes', (test) => {
    const entryCode = `export {foo} from "../foo";`;
    
    const modules = {
        'basic/bar/entry.ts': entryCode,
        'basic/foo.ts': `export const foo: number = 5;`,
        'tsconfig.json': JSON.stringify({
            compilerOptions: {
                module: 'ESNext'
            }
        })
    };
    
    const volume = Volume.fromJSON(modules);
    
    const existsSync = volume.existsSync.bind(volume);
    const readFileSync = volume.readFileSync.bind(volume);

    stub(volume, "existsSync").callsFake((path) => {
        if (path === 'basic\\bar\\entry.ts') {
            return true;
        }
        
        return existsSync(path);
    });

    stub(volume, "readFileSync").callsFake((path) => {
        if (path === 'basic\\bar\\entry.ts') {
            return readFileSync('basic/bar/entry.ts');
        }

        return readFileSync(path);
    });
    
    return runTest(test, 'basic\\bar\\entry.ts', modules, {
        expectation: {
            foo: 5
        },
        volume
    });
});