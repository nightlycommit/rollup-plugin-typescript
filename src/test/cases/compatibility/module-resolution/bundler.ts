import tape from "tape";

import {runTest} from "../../../helpers/index";

tape('honors Bundler module resolution', ({test}) => {
    const extensions = ['js', 'ts'];

    for (const extension of extensions) {
        test(extension, ({test}) => {
            test('through direct access', (test) => {
                return runTest(test, 'entry.ts', {
                    'entry.ts': `export {foo} from "foo";`,
                    'tsconfig.json': JSON.stringify({
                        compilerOptions: {
                            module: 'esnext',
                            moduleResolution: 'bundler'
                        }
                    }),
                    [`node_modules/foo.${extension}`]: `export const foo = 5;`,
                }, {
                    expectation: {
                        foo: 5
                    }
                });
            });

            test('through the main directive', (test) => {
                return runTest(test, 'entry.ts', {
                    'entry.ts': `export {foo} from "foo";`,
                    'tsconfig.json': JSON.stringify({
                        compilerOptions: {
                            module: 'esnext',
                            moduleResolution: 'bundler'
                        }
                    }),
                    'node_modules/foo/package.json': JSON.stringify({
                        main: `./dist/index.${extension}`
                    }),
                    [`node_modules/foo/dist/index.${extension}`]: `export const foo = 5;`,
                }, {
                    expectation: {
                        foo: 5
                    }
                });
            });

            test('through the exports directive', (test) => {
                return runTest(test, 'entry.ts', {
                    'entry.ts': `export {foo} from "foo";`,
                    'tsconfig.json': JSON.stringify({
                        compilerOptions: {
                            module: 'esnext',
                            moduleResolution: 'bundler'
                        }
                    }),
                    'node_modules/foo/package.json': JSON.stringify({
                        exports: {
                            'default': `./dist/index.${extension}`
                        }
                    }),
                    [`node_modules/foo/dist/index.${extension}`]: `export const foo = 5;`,
                }, {
                    expectation: {
                        foo: 5
                    }
                });
            });
        });
    }
});