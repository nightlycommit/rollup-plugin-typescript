import {createTypeScriptPlugin} from "@vitrail/rollup-plugin-typescript";
import {dirname, join} from "path";
import FsExtra from "fs-extra";

const {emptyDirSync} = FsExtra;

/**
 * @type {Array<import('rollup').RollupOptions>}
 */
const configurations = [
    {
        input: 'index.ts',
        plugins: [
            {
                generateBundle: (options) => {
                    const destination = dirname(options.file);

                    emptyDirSync(destination);
                }
            },
            createTypeScriptPlugin()
        ],
        output: {
            format: "commonjs",
            file: join('target', 'index.js'),
            sourcemap: true
        }
    }
];

export default configurations;