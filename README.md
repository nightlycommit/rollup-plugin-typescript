# @vitrail/rollup-plugin-typescript
[![NPM version][npm-image]][npm-url] [![Build Status][build-image]][build-url] [![Coverage percentage][coveralls-image]][coveralls-url]

A Rollup plugin for seamless integration between Rollup and TypeScript.

## Prerequisites

This projects needs at least **node.js 16.0.0** to run.

It is also strongly recommended to have [ts-node](https://www.npmjs.com/package/ts-node) and [nyc](https://www.npmjs.com/package/nyc) installed globally to ease the writing of tests and the tracking of the code coverage.

## Usage

### Installation

```shell
npm install
```

### Build the library

```shell
npm run build:main
```

### Build and run the test suite

```shell
npm run build:test
npm run test
```

### Writing and executing tests

Assuming one want to execute the test located in `test/cases/foo.ts`, one would run:

```shell
ts-node test/cases/foo.ts
```

It is even possible - and recommended - to track the coverage while writing tests:

```shell
odz ts-node test/cases/foo.ts
```

Of course, it is also perfectly possible to pipe the result of the test to your favorite tap formatter:

```shell
test/cases$ ts-node . | tap-nyan
 9   -_-_-_-_-_,------,
 0   -_-_-_-_-_|   /\_/\ 
 0   -_-_-_-_-^|__( ^ .^) 
     -_-_-_-_-  ""  "" 
  Pass!
```

## Contributing

* Fork this repository
* Code
* Implement tests using [tape](https://github.com/substack/tape)
* Issue a pull request keeping in mind that all pull requests must reference an issue in the issue queue

## License

Copyright © 2023 [Eric MORAND](https://github.com/ericmorand). Released under the [2-Clause BSD License](https://github.com/ericmorand/twing/blob/master/LICENSE).

[npm-image]: https://badge.fury.io/js/@vitrail%2Frollup-plugin-typescript.svg
[npm-url]: https://npmjs.org/package/@vitrail/rollup-plugin-typescript
[build-image]: https://gitlab.com/vitrail/rollup-plugin-typescript/badges/main/pipeline.svg
[build-url]: https://gitlab.com/vitrail/rollup-plugin-typescript/-/pipelines
[coveralls-image]: https://coveralls.io/repos/gitlab/vitrail/rollup-plugin-typescript/badge.svg
[coveralls-url]: https://coveralls.io/gitlab/vitrail/rollup-plugin-typescript